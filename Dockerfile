FROM node:18
WORKDIR /app
COPY ./node-hello-world/package*.json /app
RUN npm install
COPY ./node-hello-world/* /app
EXPOSE 3000
CMD ["npm", "start"]
