# 1. Build node.js app and push docker image to docker hub using Jenkinsfile [pipeline]

### jenkins configuration:
- run jenkins/jenkins:lts docker image
- do the initial setup
- install necessary plugins

### jenkins pipeline configuration:
- create pipeline script from SCM
- for gitlab webhook to trigger for each new commit:
  - create webhook in gitlab to be triggered on push events
  - add jenkins url and secret token to webhook
- add dockerhub credentials
- add path to Jenkinsfile
- add repo to cd into

### The Jenkins pipeline will:
- clone the repo
- build the node.js app
- build the docker image
- login to docker hub
- push the docker image to docker hub

Now, the pipeline will be triggered on each new commit to the repo and push the latest docker image of the node.js app to docker hub.

# 2. Build the maven app and push docker image to docker hub using Gitlab CI and own runner
  
### gitlab runner configuration:
- disable shared runners
- create a new runner and add tags
- install gitlab-runner on a VM or local machine
- register the runner using the token provided by gitlab
- start the runner to be able to run jobs

### gitlab CI configuration:
- create .gitlab-ci.yml file in the root of the repo
- add the stages and jobs
- add the dockerhub credentials as variables in gitlab
- add the runner tag to each stage
- ensure the VM running the runner has the necessary tools installed (docker, maven, java, etc.)

### The gitlab CI will:
- automatically trigger on each new commit to the repo
- build the maven app
- build the docker image
- login to docker hub
- push the docker image to docker hub

Now, the CI will be triggered on each new commit to the repo and push the latest docker image of the maven app to docker hub.
